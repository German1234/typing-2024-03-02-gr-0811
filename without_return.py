from typing import NoReturn


def black_hole() -> NoReturn:
    raise Exception("There is no going back...")


def play(player_name: str) -> None:
    print(f"{player_name} plays")


if __name__ == "__main__":
    play("Dmitry")
