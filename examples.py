import math

pi: float = math.pi
nothing: str


def headline(
        text,       #  type: str
        align=True  #  type: bool
):                  #  type (...) -> str
    if align:
        return f"{text.title()}\n{"-" * len(text)}"
    else:
        return f"{text.title()}".center(80, "o")


def circumference(radius):
    #  type: (float) -> float
    return 2 * math.pi * radius


if __name__ == "__main__":
    print(headline("python type checking"))
    print(headline("use mypy", align=True))
    print(__annotations__)
    print(circumference.__annotations__)
